const { message } = require('../../constant');
const { UnauthorizedError, InternalServerError, ResourceNotFoundError } = require('../../constant/error');
const Xendit = require('xendit-node');
const https = require('https');
const QueryString = require('qs');

const x = new Xendit({
  secretKey: process.env.XENDIT_SECRET_KEY
});
const { VirtualAcc } = x;
const vaSpecificOptions = {};
const va = new VirtualAcc(vaSpecificOptions);

const getVABanks = async (headers) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);
  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);

  const payload = await va.getVABanks();

  return payload;
};

const getVAPayment = async (headers, query) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);
  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);
  
  const payload = await va.getVAPayment({
    paymentID: query.paymentID
  });

  return payload;
};

const getFixedVA = async (headers, params) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);
  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);

  const payload = await va.getFixedVA({
    id: params.id
  });

  return payload;
};


const openVA = async (headers, body) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);
  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);

  const dateNow = new Date(Date.now());
  const dateNowAddDays = new Date(dateNow.setDate(dateNow.getDate() + 1));
  const payload = await va.createFixedVA({
    externalID: body.externalID,
    bankCode: body.bankCode,
    name: body.name,
    expectedAmt: body.expectedAmt,
    expirationDate: dateNowAddDays, // set + 1 day
    isSingleUse: true
  }).catch(e => {
    console.log(e)
    throw new InternalServerError(`VA creation failed with message: ${e.message}, Code: ${e.code}`);
  });

  return payload;
};

const closeVA = async (headers, body) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);
  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);

  // const xenditPayment = getHelper('/api/paymentGateway/xendit/payment', params, objJSON => {
  //   return objJSON;
  // })

  // console.log(xenditPayment);
  const dateNow = new Date(Date.now());
  const dateNowAddDays = new Date(dateNow.setDate(dateNow.getDate() + 1));

  const payload = await va.createFixedVA({
    externalID: body.externalID,
    bankCode: body.bankCode,
    name: body.name,
    expectedAmt: body.expectedAmt,
    isClosed: true,
    expirationDate: dateNowAddDays, // set + 1 day
    isSingleUse: true
  }).catch(e => {
    console.log(e)
    throw new InternalServerError(`VA creation failed with message: ${e.message}, Code: ${e.code}`);
  });

  return payload;
};


const updateFixedVA = async (headers, params, body) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);
  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);
  console.log(body.expectedAmt)
  const payload = await va.updateFixedVA({
    id: params.id,
    expectedAmt: body.expectedAmt
  }).catch(e => {
    console.log(e)
    throw new InternalServerError(`VA update failed with message: ${e.message}, Code: ${e.code}`);
  });

  return payload;
};

const debug = async (headers, query, body) => {

  const payload = { "TEST": "TEST" };

  return payload;
};

const getHelper = async (path, query, callback) => {
  /* Can create a helper ----------------------------------------------------------- */
  const queryString = QueryString.stringify(query);

  const options = {
    url: process.env.MHB_API_BASE_URL,
    path: `${path}?${queryString}`,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': data.length
    }
  }

  const request = http.request(options, (response) => {
    console.log(response);
    // let all_chunks = [];
    let data = '';
    response.on('data', (item) => {
      data += item;
      console.log(item)
      //   all_chunks.push(item);
    });

    response.on('end', () => {
      //   let response_body = Buffer.concat(all_chunks);
      //   console.log(response_body.toString());
      callback(JSON.parse(data));
    });

    response.on('error', (error) => {
      console.log(error.message);
    });
  });

  request.on('error', (error) => {
    console.log(error.message);
  });

  request.end();
  /* ----------------------------------------------------------- */
}

const vaService = {
  getVABanks,
  getVAPayment,
  getFixedVA,
  openVA,
  closeVA,
  updateFixedVA,
  debug
};

module.exports = vaService;
