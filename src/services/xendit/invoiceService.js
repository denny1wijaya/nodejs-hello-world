const { message } = require('../../constant');
const { UnauthorizedError, InternalServerError, ResourceNotFoundError } = require('../../constant/error');
const Xendit = require('xendit-node');
const https = require('https');
const QueryString = require('qs');

const x = new Xendit({
  secretKey: process.env.XENDIT_SECRET_KEY
});
const { Invoice  } = x;
const invoiceSpecificOptions = {};
const i = new Invoice(invoiceSpecificOptions);

const createInvoice = async (headers, body) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);
  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);

  const payload = await i.createInvoice({
    externalID: body.externalID,
    amount: body.amount,
    payerEmail: body.payerEmail,
    description: body.description
  }).catch(e => {
    console.log(e)
    throw new InternalServerError(`Invoice creation failed with message: ${e.message}, Code: ${e.code}`);
  });

  return payload;
};


const getInvoice = async (headers, params) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);
  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);

  const payload = await i.getInvoice({
    invoiceID: params.invoiceId
  }).catch(e => {
    console.log(e)
    throw new InternalServerError(`Invoice creation failed with message: ${e.message}, Code: ${e.code}`);
  });

  return payload;
};

const invoiceService = {
  createInvoice,
  getInvoice
};

module.exports = invoiceService;
