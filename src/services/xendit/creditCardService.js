const { message } = require('../../constant');
const { UnauthorizedError, InternalServerError, ResourceNotFoundError } = require('../../constant/error');
const Xendit = require('xendit-node');
const { cryptoUtil } = require('../../util');

const x = new Xendit({
  secretKey: process.env.XENDIT_SECRET_KEY
});
const { Card } = x;
const cardSpecificOptions = {};
const card = new Card(cardSpecificOptions);

const createToken = async (headers, tokenData) => {
  // const checkApiNetCore = process.env.MHB_API_BASE_URL;
  // console.log(checkApiNetCore)

  const payload = { "TEST": "TEST" };
  return payload;
};

// const getToken = async (tokenId) => {
// GET https://api.xendit.co/credit_card_tokens/:credit_card_token_id
// };

const creditCardAuthorization = async (headers, body) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);

  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);

  const payload = await card.createAuthorization({
    externalID: body.externalID,
    tokenID: body.tokenID,
    amount: body.amount,
    authID: body.authID
  });

  return payload;
};

const creditCardCreateCharge = async (headers, body) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);

  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);

  console.log(body)

  const payload = await card.createCharge(body)
    .catch(e => {
      console.log(e)
      throw new InternalServerError(`Charge creation failed with message: ${e.message}, Code: ${e.code}`);
    });

  return payload;

};

const creditCardSafeAcceptance = async (headers, body) => {
  const reqSecretKey = Buffer.from(headers["authorization"].split(" ")[1], 'base64').toString().slice(0, -1);

  if (reqSecretKey !== process.env.XENDIT_SECRET_KEY) throw new UnauthorizedError(message.WRONG_XENDIT_SECRET_KEY);

  console.log(body)
  const sharedSecret = cryptoUtil.createHash256(process.env.XENDIT_SECRET_KEY);
  const stringToSign = JSON.stringify(body);
  const signature = cryptoUtil.createHmac256(sharedSecret, stringToSign);



  return payload;

};


const creditCardService = {
  createToken,
  creditCardAuthorization,
  creditCardCreateCharge,
  creditCardSafeAcceptance
};

module.exports = creditCardService;
