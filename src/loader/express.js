const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const path = require('path');
const routes = require('../routes');

const expressLoader = async ({ app }) => {
  // Settings
  app.enable('trust proxy');

  // Middleware
  app.use(cors());
  app.use(express.json());
  app.use(morgan('tiny'));

  // Static files
  app.use(express.static(path.join(__dirname, '../static')));

  // Routes
  app.use('/xendit', routes.xenditCreditCard);
  app.use('/xendit/va', routes.xenditVA);
  app.use('/xendit/invoice', routes.xenditInvoice);
  app.use('/status', routes.status);
  app.get('/', routes.main.root);
  app.use(routes.main.notFound);
  app.use(routes.main.errorHandler);

  return app;
};

module.exports = expressLoader;
