require('dotenv/config');
const expressLoader = require('./express');

const init = async ({ expressApp }) => {
  await expressLoader({ app: expressApp });
  console.log('Express Initialized');
};

module.exports = init;
