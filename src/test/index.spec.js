/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const http = require('http');
const assert = require('assert');

const server = require('../src/app');

describe('Boilerplate Backend', () => {
  it('should return 200', (done) => {
    http.get('http://localhost:3000', (res) => {
      assert.strictEqual(200, res.statusCode);
      done();
    });
  });
});
