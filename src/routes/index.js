const main = require('./general/main');
const status = require('./general/status');
const xenditCreditCard = require('./xendit/creditCard');
const xenditVA = require('./xendit/va');
const xenditInvoice = require('./xendit/invoice');

module.exports = {
  main,
  status,
  xenditCreditCard,
  xenditVA,
  xenditInvoice
}