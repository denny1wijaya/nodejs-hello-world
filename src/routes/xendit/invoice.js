const { Router } = require('express');
const { celebrate } = require('celebrate');
const { invoiceService } = require('../../services');

const router = Router();

router.post('/createInvoice', async (req, res, next) => {
    try {
        const payload = await invoiceService.createInvoice(req.headers, req.body);
        console.log(payload, '===== createInvoice payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

router.get('/getInvoice/:invoiceId', async (req, res, next) => {
    try {
        const payload = await invoiceService.getInvoice(req.headers, req.params);
        console.log(payload, '===== getInvoice payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

module.exports = router;
