const { Router } = require('express');
const { celebrate } = require('celebrate');
const { vaService } = require('../../services');

const router = Router();

router.get('/getVABanks', async (req, res, next) => {
    try {
        const payload = await vaService.getVABanks(req.headers);
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

router.get('/getVAPayment', async (req, res, next) => {
    try {
        const payload = await vaService.getVAPayment(req.headers, req.query);
        console.log(payload, '===== getVAPayment payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

router.get('/getFixedVA/:id', async (req, res, next) => {
    try {
        console.log(req.params)
        const payload = await vaService.getFixedVA(req.headers, req.params);
        console.log(payload, '===== getFixedVA payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

router.post('/openVA', async (req, res, next) => {
    try {
        const payload = await vaService.openVA(req.headers, req.body);
        console.log(payload, '===== openVA payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

router.post('/closeVA', async (req, res, next) => {
    try {
        const payload = await vaService.closeVA(req.headers, req.body);
        console.log(payload, '===== closeVA payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});


router.patch('/updateFixedVA/:id', async (req, res, next) => {
    try {
        const payload = await vaService.updateFixedVA(req.headers, req.params, req.body);
        console.log(payload, '===== updateFixedVA payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

router.post('/debug', async (req, res, next) => {
    try {
        console.log(req, '========== req')
        console.log(req.headers, '========== req headers')
        console.log(req.body, '========== req body')
        console.log(req.params, '========== req params')
        console.log(req.query, '========== req query')
        const payload = await vaService.debug(req.headers, req.query, req.body);
        console.log(payload, '===== debug payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

module.exports = router;
