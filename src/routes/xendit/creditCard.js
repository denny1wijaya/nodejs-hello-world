const { Router } = require('express');
const { celebrate } = require('celebrate');
const { creditCardService } = require('../../services');

const router = Router();

router.post('/createToken', async (req, res, next) => {
    try {
        const payload = await creditCardService.createToken(req.headers, req.body);
        console.log(payload, '===== routes')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

router.post('/creditCardAuthorization', async (req, res, next) => {
    try {
        const payload = await creditCardService.creditCardAuthorization(req.headers, req.body);
        console.log(payload, '===== creditCardAuthorization payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

router.post('/creditCardCreateCharge', async (req, res, next) => {
    try {
        const payload = await creditCardService.creditCardCreateCharge(req.headers, req.body);
        console.log(payload, '===== creditCardCreateCharge payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

router.post('/creditCardSafeAcceptance', async (req, res, next) => {
    try {
        const payload = await creditCardService.creditCardSafeAcceptance(req.headers, req.body);
        console.log(payload, '===== creditCardSafeAcceptance payload')
        res.json({ payload: payload });
    } catch (e) {
        next(e);
    }
});

module.exports = router;
