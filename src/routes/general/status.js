const { Router } = require('express');

const router = Router();

router.get('/server', (req, res) => {
  res.status(200).json({ status: 'alive' });
});

module.exports = router;
