const { message } = require('../../constant');
const { PathNotFoundError } = require('../../constant/error');

const root = (req, res) => {
    res.sendFile(message.WELCOME);
};

const notFound = (req, res, next) => {
    console.log(`${req.ip} tried to access ${req.originalUrl}`);
    next(new PathNotFoundError());
};

const errorHandler = (error, req, res, next) => {
    console.error(`Global error handler: ${error}`);

    const httpStatusCode = error.statusCode || 500;
    res.status(httpStatusCode).json({ [error.name]: error.message });
    next();
};

module.exports = { root, notFound, errorHandler };
