const express = require('express');
const init = require('./loader')
const startServer = async () => {
    const app = express();
    
    const port = process.env.PORT || 3000;

    await init({ expressApp: app });

    app.listen(port, () => {
        console.log(`Hello World Backend listening on port ${port}!`);
    });

    return app;
};

startServer();
