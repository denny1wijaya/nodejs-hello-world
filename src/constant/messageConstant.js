const message = {
  WELCOME: 'Welcome to Boilerplate Template 👨‍🏭',
  WRONG_EMAIL_OR_PASSWORD: 'Email/Password is wrong',
  UNABLE_TO_UPDATE_DIFFERENT_USER: 'Unable to update different User.',
  WRONG_XENDIT_SECRET_KEY: 'Wrong Xendit Secret Key Unable to use Xendit.',
  MHB_NAME: "mHealthBank"
};

module.exports = message;
