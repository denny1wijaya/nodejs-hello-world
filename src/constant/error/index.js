const InternalServerError =  require('./InternalServerError');
const PathNotFoundError =  require('./PathNotFoundError');
const ResourceNotFoundError =  require('./ResourceNotFoundError');
const UnauthorizedError =  require('./UnauthorizedError');

module.exports = { InternalServerError, PathNotFoundError, ResourceNotFoundError, UnauthorizedError };
