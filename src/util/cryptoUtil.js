const bcrypt = require('bcrypt');
const crypto = require('crypto');

const saltRounds = 10;
const hash = async (text) => bcrypt.hash(text, saltRounds);
const compare = async (text, hashed) => bcrypt.compare(text, hashed);
const createHash256 = async(hashed) => 
                      crypto.createHash('sha256')
                            .update(hashed)
                            .digest('hex');
const createHmac256 = async(shared_secret, string_to_sign) => 
                      crypto.createHmac('sha256', shared_secret)
                            .update(string_to_sign)
                            .digest('hex');

module.exports = { compare, hash, createHash256, createHmac256 };
